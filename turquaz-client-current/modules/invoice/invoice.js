Core.register("invoice", function(sandbox) {
	ver = "1.0";
	return {
		init : function() {
			this.createMenu();

			sandbox.addListener("addInvoce", this.addCurrent, this);

	        require(["modules/invoice/pages/addInvoice"], function() {
	            require.ready(function() {
	            });
	        });
		},
		
		destroy : function() {
			$("#invoice").remove();
		},
		
		addCurrent : function(params){
			var current = $$(invoiceModel, invoiceView, invoiceController);

			var tabContent = {
					tabName : "Add Invoice",
					tabContent : current };

			sandbox.notifyListener("add-tab",tabContent)

		},

		createMenu: function(){
			var menus = [];

			var menu = {
				parentMenu : "invoice",
				name : "Add Invoice",
				link : "addInvoice" };
			menus.push(menu);

			var menu = {
				parentMenu : "invoice",
				name : "Search Invoice",
				link : "searchInvoice" };
			menus.push(menu);

			var menu = {
				parentMenu : "invoice",
				name : "Reports",
				link : "reports" };
			
			menus.push(menu);

			var moduleMenu = {
					name : "Fatura",
					key : "invoice"
			};
			
			sandbox.notifyListener("create-menu", moduleMenu);
			sandbox.notifyListener("add-menu-step", menus);
		}
		
	};
});