Core.register("main", function(sandbox) {
	ver = "1.0";
	return {
		/**
		 * Init will be treated as the builder of the module and 
		 * will be executed when the module is generated.
		 */	 
		init : function() {
			try {
				this.createMain();
				this.createContentArea();
				sandbox.addListener("add-menu-step", this.addMenuStep, this);
				sandbox.addListener("create-menu", this.createMenu, this);
				sandbox.addListener("add-tab", this.addTab, this);

			} catch (ex) {
				alert("exception "+ ex);
			}
		},
		/**
		 * Destroy will be treated as the destroyer of the module 
		 * running when the module is removed.
		 */

		createMain : function() {
			$('body').append("<div id='main'></div>");
			$('#main').append("<div id='menu' style='float: left;width: 97%;padding: 20px'><ul class='sf-menu' id='sf-menu'></div>");
			$('#main').append("<div id='tabs' style='float: left;width: 97%;padding: 10px'><ul></ul></div>");
		},
		
		createContentArea: function() {
			var $tabs = $( "#tabs").tabs({
				tabTemplate: "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close'>Remove Tab</span></li>",
				add: function( event, ui ) {
					var tab_content = "";
					$( ui.panel ).append( "<p>" + tab_content + "</p>" );
				}
			});
			
			// close icon: removing the tab on click
			// note: closable tabs gonna be an option in the future - see http://dev.jqueryui.com/ticket/3924
			$( "#tabs span.ui-icon-close" ).live( "click", function() {
				var index = $( "li", $tabs ).index( $( this ).parent() );
				$tabs.tabs( "remove", index );
			});
		},
	
		createMenu : function(params) {
			$('#sf-menu').append("<li id="+params.key+"><a href='#'>"+params.name+"</a></li>");			
		},

		tab_counter : 0,
		tabs : {},
		
		addTab: function(params){
			if (this.tabs[params.tabKey]){
				$("#tabs").tabs('option', 'selected', this.tabs[params.tabKey]);				
				
			} else {
				this.tabs[params.tabKey] = this.tab_counter;
				var tab_title = params.tabName;
				$( "#tabs").tabs( "add", "#tabs-" + this.tab_counter, tab_title );
				$$.document.append(params.tabContent,"#tabs-"+this.tab_counter);	
				this.tab_counter++;
			}
		},
		
		
		addMenuStep: function(params){
			
			for (var i=0; i<params.length; i++){
				var parentMenu = $("#"+params[i].parentMenu);
				var parentMenuSub = $("#"+params[i].parentMenu+"Sub");
				if (!($("#"+params[i].parentMenu+"Sub").text())){
					parentMenu.append("<ul id='"+params[i].parentMenu+"Sub"+"'/>");	
				}
				$("#"+params[i].parentMenu+"Sub").append("<li id='"+params[i].key+"'><a href='#"+params[i].key+"'>"+params[i].name+"</a></li>");

			}
			$('ul.sf-menu').superfish();
		},
		
		destroy : function() {
			sandbox.removeListener("add-menu-step", this);
			sandbox.removeListener("create-menu", this);
			$('body').empty();

		},
	};
});