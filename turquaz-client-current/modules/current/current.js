Core.register("current", function(sandbox) {
	ver = "1.0";
	return {
		init : function() {
			this.createMenu();
			
			CurrentRouter = Backbone.Router.extend({
				routes: {
					'addCurrent' : 'addCurrent',
					'cariAlacakFisi' : 'searchCurrent'
				},
				
				initialize: function(){
					
				},
				
				addCurrent : function(){
			        require(["modules/current/pages/addCurrent"], function() {
			            require.ready(function() {
			    		    if (currentView.html != null){
			    				$.get(currentView.html , function(data) {
			    					currentView.format = data;

			    					var current = $$(currentModel, currentView, currentController);

			    					var tabContent = {
			    							tabKey : "addCurrent",
			    							tabName : "Add Current",
			    							tabContent : current };
			    					
			    					sandbox.notifyListener("add-tab",tabContent);
			    				}
			    		    )};

			            });
			        });
				},

				searchCurrent : function(){
			        require(["modules/current/pages/searchCurrent"], function() {
			            require.ready(function() {
			    		    if (currentCView.html != null){
			    				$.get(currentCView.html , function(data) {
			    					currentCView.format = data;

			    					var current = $$(currentCModel, currentCView, currentCController);

			    					var tabContent = {
			    							tabKey : "searchCurrent",
			    							tabName : "Search Current",
			    							tabContent : current };
			    					
			    					sandbox.notifyListener("add-tab",tabContent);
			    				}
			    		    )};

			            });
			        });
				}
			});
			
			currentRouterApp = new CurrentRouter();
			Backbone.history.start();
		},
		
		destroy : function() {
			$("#current").remove();
		},
		
		createMenu: function(){
			var menus = [];

			var moduleMenu = {
					name : "Cari",
					key : "current"
			};

			sandbox.notifyListener("create-menu", moduleMenu);

			sandbox.getService( "currents/getMenu",
								function (data) {
									sandbox.notifyListener("add-menu-step", data);
								}, this);
		}
		
	};
});