var debug = true;
/**
 * Core is a small script that allows modules to register and 
 * associate a new instance of the SandBox to each of them every time you run.
 * As we see it is a small script (this is a base, can be extended depending on 
 * the needs of the project) that allows us to build the modules we want for every application.
 */
var Core = function() {
	/**
	 * A module (in our terms) is one application that is part of a larger structure. 
	 * Each has a job and only takes care of it.
	 */
	// Private Variable
	var modules = {};

	//Listeners
	var listenerList = {};

	// Instance Creams
	function createInstance(moduleID) {
		var instance = modules[moduleID].creator(new Sandbox(Core)), name, method;

		if (!debug) {
			for (name in instance) {
				method = instance[name];
				if (typeof method == "function") {
					instance[name] = function(name, method) {
						return new function() {
							return method.apply(this, arguments);
						};
					};
				}
			}
		}
		return instance;
	}

	// Public method
	return {
		/**
		 * The first parameter informs the module name. 
		 * The second parameter specifies the functionality of the module. 
		 */
		register : function(moduleID, creator) {
			modules[moduleID] = {
				creator : creator,
				instance : null
			};
		},
		start : function(moduleID) {
			modules[moduleID].instance = createInstance(moduleID);
			modules[moduleID].instance.init();
		},
		stop : function(moduleID) {
			var data = modules[moduleID];
			if (data.instance) {
				data.instance.destroy();
				data.instance = null;
			}
		},
		startAll : function() {
			for ( var moduleID in modules) {
				if (modules.hasOwnProperty(moduleID)) {
					this.start(moduleID);
				}
			}
		},
		stopAll : function() {
			for ( var moduleID in modules) {
				if (modules.hasOwnProperty(moduleID)) {
					this.stop(moduleID);
				}
			}
		},
		addListener : function(messageType, handler, scope) {
			var listener = listenerList[messageType] || (listenerList[messageType] = []);
			listener.push({
				handler : handler,
				scope : scope
			});
		},
		removeListener : function(messageType, scope) {
			var listeners = listenerList[messageType];
			if (listeners) {
				for ( var i = 0; i < listeners.length; i++) {
					if (listeners[i]) {
						if (listeners[i].scope === scope) {
							listeners[i] = null;
						}
					}
				}
			}
		},
		notifyListener : function(messageType, params) {
			var listeners = listenerList[messageType];
			if (listeners) {
				for ( var i = 0; i < listeners.length; i++) {
					if (listeners[i]) {
						listeners[i].handler.call(listeners[i].scope,params);
					}
				}
			}
		},
		getService: function(serviceName, callback, scope) {
	        $.ajax({
	            type: "GET",
	            contentType: "application/json",
	            url: "http://localhost/"+serviceName,
	            dataType: "json",
	            success: function (data) {
	        			callback.call(scope,data);
	            }
	        });
		}

	};
}();

/**
 * In software development environment called the Sandbox to test 
 * isolated where the experts on running applications. 
 * In this case, practically based on the same and that each of 
 * the modules used an isolated environment for other modules.
 * This is an element that offers a number of ways in which we interact with the application.
 */
Sandbox = function(core) {

	this.notifyListener = function(messageType, params) {
		core.notifyListener(messageType, params);
	};

	this.getService = function(serviceName, callback, scope){
		core.getService(serviceName, callback, scope);		
	}
	this.addListener = function(messageType, handler, scope) {
		core.addListener(messageType, handler, scope);
	};

	this.removeListener = function(messageType, scope) {
		core.removeListener(messageType, scope);
	};
};